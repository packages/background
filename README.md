## Shifft background
#### What is this?
This package allows laravel to run a laravel command in the background

#### Getting Started
To get started, require the package with `composer require shifft/background` and publish the configfile with `php artisan vendor:publish --tag=config`

Please modify the `config/background.php` to match your configuration.

#### Usage
Have the following code at the top of your file
```php
use Shifft\Background\Background;
```

And use it as such:
```php
Background::runInBackround('list', 'listPid');//run the artisan list command with the process id(pid) listPid. The pid can be anything you want
Background::isRunning('listPid');//check if the process with the pid listPid is still running
```

To cleanup the pid files the commando background:cleanup was made and should be scheduled by putting the following lines in the Console Kernel schedule command.
```php
$schedule->command('background:cleanup')
		 ->dailyAt('0:00');
```