<?php
return [
	//set if the cleanup commando will remove old result files
	'cleanResult' => true,
    //control how long the result files are stored, will be ignored if cleanResult = false
    'removeAfter' => '30 days',
	//the folder inside the storage dir used to store the result and pid files
	'storageDir' => 'background',
];