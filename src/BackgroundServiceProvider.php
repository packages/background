<?php

namespace Shifft\Background;

use Illuminate\Support\ServiceProvider;
use Shifft\Background\Console\Commands\BackgroundCleanup;

class BackgroundServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(){
        // Publish config file
        $this->publishes([
            __DIR__.'/config/background.php' => config_path('background.php')
        ], 'config');

    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register(){

        // Merge configfile
        $this->mergeConfigFrom(
            __DIR__.'/config/background.php'
        , 'config');

		//add the package commands to the app
		$this->commands(
		[
			BackgroundCleanup::class,
		]);
    }
}
