<?php
namespace Shifft\Background;

class Background
{
	/**
	 *  @brief Run a laravel command in the background
	 *  
	 *  @param string $command the laravel command to run(without the 'php artisan' part)
	 *  @param string $pId the application assinged process id
	 *  @return void
	 */
	public static function runInBackround(string $command, string $pId): void
	{
		//if the process isn't running
		if(!static::isRunning($pId))
		{
			//dd($_SERVER, static::getPath('pids'));
			$pidfile = static::getPath('pids')."/".$pId.'.pid';//set the path for the file with the pId
			$outputfile = static::getPath('results')."/".date('YmdHis').'_'.$pId.'.txt';//the result path
			$base = base_path();
			$cmd = env('PHP_PATH', 'php').' '.$base."/artisan ".$command.' 2>&1';//create the command for the command line
			exec(sprintf("%s > %s 2>&1 & echo $! >> %s", $cmd, $outputfile, $pidfile));//execute the command
		}
		//throw an error
		else
		{
			throw new \Exception('proces already running');
		}
	}

	/**
	 *  @brief check if the command is already running based on the application assinged process id
	 *  
	 *  @param string $pId the application assinged process id
	 *  @return bool
	 */
	public static function isRunning(string $pId): bool
	{
		//check if there is a file for the application assinged process id
		$path = static::getPath('pids').'/'.$pId.'.pid';
		if(!file_exists($path))
		{
			return false;
		}
		$pId = file_get_contents($path);
		try{
			//check if the process is running
			$result = shell_exec(sprintf("ps %d", $pId));
			if( count(preg_split("/\n/", $result)) > 2){
				return true;
			}
		}catch(\Exception $e){}

		return false;
	}
	
	/**
	 *  @brief remove the pId file of processes that are done and remove result files that are older then 60 days
	 *  
	 *  @return void
	 */
	public static function cleanUp(): void
	{
		//get the pidfiles
		$path = static::getPath('pids');
		$files = scandir($path);
		foreach($files as $file)
		{
			//if the file is a pid file and the process is not running, remove the file
			if(substr($file, -4) == '.pid' && !static::isRunning(substr($file, 0, -4)))
			{
				unlink($path.'/'.$file);
			}
		}
		if(config('background.cleanResult'))
		{
			//get the result files
			$path = static::getPath('results');
			$files = scandir($path);
			foreach($files as $file)
			{
				//if the current file is a txt file and is older then 60 days, remove the file
				if(substr($file, -4) == '.txt' && filemtime($path.'/'.$file) < strtotime('-60 days'))
				{
					unlink($path.'/'.$file);
				}
			}
		}
	}
	
	/**
	 *  @brief get the background file path and create the folder if it doesn't exists
	 *  
	 *  @param string $folder the folder inside the background folder to return, if null the background folder gets returned
	 *  @return string
	 */
	private static function getPath(string $folder = null): string
	{
		$path = storage_path('app/'.config('background.storageDir', 'background'));
		if(!file_exists($path))
		{
			mkdir($path);
		}
		if($folder != null)
		{
			$path .= '/'.$folder;
			if(!file_exists($path))
			{
				mkdir($path);
			}
		}
		return $path;
	}
}
?>